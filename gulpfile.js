var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });
    console.log('ISS Locator');
});

gulp.task('watch', function(){
  gulp.watch("src/**/*.html").on('change', browserSync.reload);
  gulp.watch("src/**/*.php").on('change', browserSync.reload);
  // gulp.watch("src/content/*.html").on('change', browserSync.reload);
  gulp.watch("src/js/*.js").on('change', browserSync.reload);
  // gulp.watch('src/scss/*.scss', gulp.series('sass'));
});

gulp.task('js', function() {
  return gulp.src([
      'node_modules/mappa-mundi/dist/mappa.js',
      'node_modules/p5/lib/p5.min.js',
      'node_modules/jquery/dist/jquery.min.js'
    ])
    .pipe(gulp.dest('./src/js/'))
    .pipe(browserSync.stream());
});

gulp.task('default',  gulp.parallel('watch', 'browser-sync'));
