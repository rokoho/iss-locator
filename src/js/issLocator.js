// import { getLatLngObj } from "/js/sgp4.js";
let myMap;
let canvas;
let issPos = {x:0,y:0};
let lastPos;
let pathData = [];
const mappa = new Mappa('Leaflet');
let contentHeight = document.getElementById('canvas').offsetHeight;
let issPng;

// Lets put all our map options in a single object
const options = {
  lat: 0,
  lng: 0,
  zoom: 2,
  style: "http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png"
}

function windowResized() {
  resizeCanvas(windowWidth, contentHeight);
}

function setup() {
  // frameRate(25);

  issPng = loadImage('/img/iss.png');
  let renderer = createCanvas(windowWidth, contentHeight);
  renderer.parent("canvas");
  // Create a tile map with the options declared
  myMap = mappa.tileMap(options);
  var timer = setInterval(getIssData,2000);
  getIssData();
  myMap.onChange(() => {
    clear();
    if (lastPos !== undefined) {
      issPos = myMap.latLngToPixel(lastPos.latitude,lastPos.longitude);
    }
    drawIss(issPos.x,issPos.y);
  });
  myMap.overlay(renderer);
}

function draw(){
  // console.log(pos);
  if (issPos.x) {
    // drawIss(issPos.x,issPos.y);
  }




}

function drawPath(){
  pathData.forEach((data,id) => {
    if (id > 1) {
      let prevPos = myMap.latLngToPixel(pathData[id-1].latitude,pathData[id-1].longitude);
      let thisPos = myMap.latLngToPixel(pathData[id].latitude,pathData[id].longitude);
      stroke(200);
      if (prevPos.x < thisPos.x) {
        line(prevPos.x,prevPos.y,thisPos.x,thisPos.y);
      }
  }
  });
}

function displayTelemetry(data){
  var crX = 50;
  var crY = height- 130;
  ln = 15;
  fill(255);
  text('ISS telemetry',crX,crY);
  text('Velocity: ' + Math.ceil(data.velocity) + ' km/h',crX,crY+ln);
  text('Altitude: ' + Math.ceil(data.altitude)+' km',crX,crY+(ln*2));
  text('Latitude: '  + data.latitude,crX,crY+(ln*3));
  text('Longitude: '  + data.longitude,crX,crY+(ln*4));
  text('Visibility: '  + data.visibility,crX,crY+(ln*5));
}

function drawIss(x,y){
  fill(200,0,0);
  imageMode(CENTER);
  image(issPng,x,y,20*myMap.getZoom(),20*myMap.getZoom())
  // ellipse(x,y,5);
  // drawPath();
}

// function drawOrbit(){
//   console.log('orbit');
//   getJsonData('https://api.wheretheiss.at/v1/satellites/25544/tles',(data) => {
//     console.log(data);
//   })
// }

function getJsonData(url,cb){
  var jqxhr = $.getJSON(url)
  .done(function(data) {
    console.log( "second success" );
    cb(data);
    return data;
  })
  .fail(function() {
    console.log( "error" );
  })
  .always(function() {
    console.log( "complete" );
  });
}
function getIssData(cb){
  console.log('getting');
  var jqxhr = $.getJSON( "https://api.wheretheiss.at/v1/satellites/25544", function(data) {
    issPos = myMap.latLngToPixel(data.latitude,data.longitude);
    lastPos = data;
    pathData.push(data);
    console.log(data);
    clear();

    displayTelemetry(lastPos);
    drawIss(issPos.x,issPos.y);
    // myMap.map.flyTo([data.latitude,data.longitude],2);
    // cb();
  })
  .done(function() {
    console.log( "second success" );
  })
  .fail(function() {
    console.log( "error" );
  })
  .always(function() {
    console.log( "complete" );
  });

  // Perform other work here ...

  // Set another completion function for the request above
  jqxhr.always(function() {
    console.log( "second complete" );
  });
}
